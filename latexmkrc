@default_files = ('grundlagen-systemintegration.tex', 'grundlagen-systemintegration-book.tex');
$pdf_mode = 1;

add_cus_dep( 'acn', 'acr', 0, 'makeglossaries' );
add_cus_dep( 'glo', 'gls', 0, 'makeglossaries' );
add_cus_dep( 'glo-abr', 'gls-abr', 0, 'makeglossaries' );
$clean_ext .= " acr acn alg glo gls glg";
sub makeglossaries {
    my ($name, $path) = fileparse( $$Psource );
    return system "makeglossaries -d \"$path\" \"$name\"";
}
