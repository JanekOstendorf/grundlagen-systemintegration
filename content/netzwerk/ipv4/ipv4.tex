\chapter{Internet Protocol Version 4 (IPv4)}

\section{IPv4-Adressen}

Eine Adresse im \ab{ipv4} hat eine Länge von \SI{4}{Byte}.
Die Adresse teilt sich in \Intro{vier Oktette}\index{Oktett} auf, die jeweils eine Länge von einem Byte, also acht Bit haben. 
Die Oktette werden durch jeweils einen Punkt voneinander getrennt.
In \cref{fig:ipv4-adressen-schreibweisen} ist die Schreibweise der \as{ipv4}"=Adressen dargestellt.

\begin{figure}
    \caption{Schreibweisen von IPv4"=Adressen}
    \label{fig:ipv4-adressen-schreibweisen}
    \centering
    \subimport{images/}{ipv4-address.tex}
\end{figure}

\intro[führende Nullen auslassen]{Führende Nullen} in den einzelnen Oktetten können ausgelassen werden.
Die IP"=Adresse \code{192.051.100.012} kann also auch als \code{192.51.100.12} geschrieben werden.

Dadurch, dass die Adressen eine Länge von \SI{32}{Bit} haben, lassen sich mit ihnen \intro[über 4 Mio. Adressen]{insgesamt \num{4294967296}} ($2^{32}$) verschiedene Adressen abbilden.
In jedem Oktett stehen \num{256} ($2^{8}$) Möglichkeiten zur Verfügung, also ein Bereich von \numrange{0}{255}.

\begin{table}
    \caption{Minimale und maximale IP-Adressen}
    \label{fig:ipv4-adressen-min-max}
    \centering

    \begin{tabular}{llc@{}c@{}c@{}c@{}c@{}c@{}c}
        \toprule
                                & \thead{Format}    & \multicolumn{7}{c}{Adresse}                                       \\ \midrule
        \multirow{2}{*}{Min.}   & Binär             & \code{0000\,0000} & \code{.} & \code{0000\,0000} & \code{.} & \code{0000\,0000} & \code{.} & \code{0000\,0000}    \\
                                & Dezimal           & \code{000} & \code{.} & \code{000} & \code{.} & \code{000} & \code{.} & \code{000}                                \\[1ex]
        \multirow{2}{*}{Max.}   & Binär             & \code{1111\,1111} & \code{.} & \code{1111\,1111} & \code{.} & \code{1111\,1111} & \code{.} & \code{1111\,1111}    \\
                                & Dezimal           & \code{255} & \code{.} & \code{255} & \code{.} & \code{255} & \code{.} & \code{255}                                \\ \bottomrule
    \end{tabular}
\end{table}

\section{IP-Netzwerke}

Aufgabe des \al{ip}s ist es, Daten zwischen verschiedenen Netzwerken zu transportieren.
Diese Netzwerke bestehen aus einem bestimmten Bereich von IP"=Adressen, einem \sog{} \Introf*{Subnet} oder \Intro{Subnetz}.
Um solche Netzwerke zu definieren, werden IP"=Adressen in zwei Teile aufgeteilt: \Intro{Netzanteil und Hostanteil}.
Der Netzanteil identifiziert das Netzwerk \bzw{} Subnetz und der Hostanteil einen bestimmten Host in diesem Netzwerk, also einen Teilnehmer oder Computer im Netzwerk.

Der Netzanteil steht am Anfang einer IP"=Adresse.
Die Bits in der Subnetzmaske sind im Netzanteil immer \code{1}. \marginnote{Netzanteil \code{1}, Hostanteil \code{0}}
Im Hostanteil sind die Bits der Subnetzmaske \code{0}.
In \cref{fig:ipv4-adressen-netz-hostanteil} ist eine IP"=Adresse mit zugehöriger Subnetzmaske abgebildet.
In diesem Beispiel hat der Netzanteil eine Länge von 24 Bits, da die ersten 24 Bits der Subnetzmaske \code{1} sind.
Die letzten 8 Bits sind der Hostanteil.
Diese Bits sind in der Subnetzmaske \code{0}.

\begin{figure}
    \caption{Netz- und Hostanteil in IP"=Adressen und Subnetzmasken}
    \label{fig:ipv4-adressen-netz-hostanteil}
    \centering
    \subimport{images/}{ipv4-subnetmask.tex}
\end{figure}

In den Anfängen des Internets waren die Netzwerke und ihre Größen klar definiert.
Die zur Verfügung stehenden IP"=Adressen wurden eingeteilt in Klasse"=A-, Klasse"=B- und Klasse"=C"=Netze.
\intro{Klasse"=A"=Netze} mit einer Maske von \code{255.0.0.0} gab es im Bereich von \code{0.0.0.0} bis \code{127.255.255.255}.
In Klasse"=A"=Netzen wurde das erste Oktett für den Netzanteil verwendet.
Die anderen Oktette konnten für Hosts verwendet werden, womit insgesamt mehr als 16 Millionen Host ein einem Netz adressiert werden konnten.

Darüber kamen \intro{Klasse"=B"=Netze} von \code{128.0.0.0} bis \code{191.255.255.255} und einer Maske von \code{255.255.0.0}.
In diesen Netzen wurden zwei Oktette für den Netzbereich und zwei Oktette für den Hostbereich verwendet.

\intro{Klasse"=C"=Netze} von \code{192.0.0.0} bis \code{223.255.255.255} hatten eine Maske von \code{255.255.255.0} und waren damit die kleinsten Netzwerke.
Hier wurden drei Oktette für den Netzbereich und nur ein Oktett für den Hostbereich vorgesehen.

Je nachdem, in welchem Adressbereich eine IP"=Adresse liegt, konnte man also direkt die Subnetzmaske bestimmen.
Die Beispieladresse \code{192.51.100.12} liegt im Bereich der Klasse"=C"=Netze und hat somit eine Maske von \code{255.255.255.0}.

Um die Schreibweise von Subnetzmasken zu verkürzen, werden diese häufig in der \Intro{Präfix-Darstellung} angegeben.
Dabei wird die binäre Subnetzmaske nicht wie üblich in dezimale Zahlen umgewandelt.
Stattdessen wird die Anzahl der Bits angegeben, die für den Netzbereich verwendet werden. 
Eine Subnetzmaske von \code{255.255.255.0} entspricht also einem Präfix von \code{/24},
eine Maske von \code{255.0.0.0} einem Präfix von \code{/8}.

Um ein Netzwerk eindeutig zu identifizieren, muss man ihren Netzanteil an einer IP"=Adresse kennen.
Bei einer solchen Angabe werden alle Bits aus dem Host"=Anteil auf \code{0} gesetzt.
So erhält man die \Intro{Netzadresse} eines Netzwerkes.
Zusammen mit der Subnetzmaske \bzw{} dem Präfix kann ein Netzwerk eindeutig identifiziert werden.
Die Netzadresse aus unserem Beispiel -- zusammen mit dem Präfix -- ist so \code{192.168.51.0/24}.

\section{Subnetting}

Schon in den frühen Anfängen des Internets mussten große Netzwerke in mehrere kleinere Netzwerke aufgeteilt werden.
Große Klasse"=A"=Netze wurden meist großen Unternehmen zugewiesen (\zB{} Apple oder Ford).
Um die Adressen an verschiedenen Standorten nutzen zu können, mussten die großen Netzwerke aufgeteilt werden.

Um das zu erreichen, wurde von den eigentlich festgelegten festen Subnetzmasken abgewichen.
Dadurch konnte der Netzanteil der IP"=Adressen vergrößert werden, wodurch neue Netzwerke definiert werden konnten.
Aus einem Klasse"=A"=Netzwerk mit Präfix \code{/8} konnten durch Vergrößerung des Netzanteils auf \code{/10} vier kleinere Netzwerke geschaffen werden.